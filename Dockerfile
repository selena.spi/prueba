FROM debian:10

RUN apt-get update && apt-get install --no-install-recommends apache2 -y && apt-get clean && rm -rf /var/lib/apt/lists/*

EXPOSE 80

ENTRYPOINT ["/usr/sbin/apache2"]
CMD ["-D", "FOREGROUND"]
